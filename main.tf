provider "aws" {
  region = "us-east-2"
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
}

variable "cidr_blocks" {
  description = "cidr blocks for vpc and subnet"
  type = list(object({
    cidr_block = string 
    name = string
    }))
}
variable aws_access_key {}
variable aws_secret_key {}

resource "aws_vpc" "development-vpc" {
  cidr_block = var.cidr_blocks[0].cidr_block 
  tags = {
    "Name" = var.cidr_blocks[0].name 
  }
}

resource "aws_subnet" "dev-subnet-1" {
  vpc_id = aws_vpc.development-vpc.id
  cidr_block = var.cidr_blocks[1].cidr_block 
  availability_zone = "us-east-2a"
  tags = {
    "Name" = var.cidr_blocks[1].name 
  }
}
